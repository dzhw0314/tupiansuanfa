// PicZoom.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "zoom.h"
#include <windows.h>

int main()
{
	TPicRegion pic, pic2;
	bool bRet = PicRead(&pic, "1.bmp");
	
	pic2.width = 32;
	pic2.height = 32;

	PicZoom2(pic2, pic);
	PicWrite(&pic2, "2.bmp");
	system("pause");
    return 0;
}
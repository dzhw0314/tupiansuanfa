#include "zoom.h"
#include <stdio.h>
#include <windows.h>

//那么访问一个点的函数可以写为：
inline TARGB24& Pixels24(const TPicRegion& pic, const long x, const long y)
{
	return ((TARGB24*)((TUInt8*)pic.pdata + pic.byte_width*y))[x];
}

inline TARGB32& Pixels32(const TPicRegion& pic, const long x, const long y)
{
	//printf("x = %d y = %d index = %d picSize = %d\n", x, y, pic.byte_width*y + x, pic.picSize);
	return ((TARGB32*)((TUInt8*)pic.pdata + pic.byte_width*y))[x];
}

#define PixCopy(dst, x, y, src, srcx, srcy, bitCount)\
{\
	if(bitCount == 24)\
	{\
		Pixels24(Dst, x, y) = Pixels24(Src, srcx, srcy);\
	}\
	else\
	{\
		Pixels32(Dst, x, y) = Pixels32(Src, srcx, srcy);\
	}\
}

bool TPicRegion_Init(TPicRegion & pic)
{
	pic.byte_width = pic.width * pic.bitCount / 8;
	pic.picSize = pic.width * pic.height * pic.bitCount / 8;
	pic.pdata = new char[pic.picSize];

	return true;
}

void PicZoom0(TPicRegion& Dst, const TPicRegion& Src)
{
	if ((0 == Dst.width) || (0 == Dst.height)
		|| (0 == Src.width) || (0 == Src.height)) return;

	Dst.bitCount = Src.bitCount;
	TPicRegion_Init(Dst);

	for (long x = 0; x<Dst.width; ++x)
	{
		for (long y = 0; y<Dst.height; ++y)
		{
			long srcx = (x*Src.width / Dst.width);
			long srcy = (y*Src.height / Dst.height);
			PixCopy(Dst, x, y, Src, srcx, srcy, Src.bitCount);
		}
	}
}

void PicZoom1(TPicRegion& Dst, const TPicRegion& Src)
{
	if ((0 == Dst.width) || (0 == Dst.height)
		|| (0 == Src.width) || (0 == Src.height)) return;

	Dst.bitCount = Src.bitCount;
	TPicRegion_Init(Dst);

	for (long y = 0; y<Dst.height; ++y)
	{
		for (long x = 0; x<Dst.width; ++x)
		{
			long srcx = (x*Src.width / Dst.width);
			long srcy = (y*Src.height / Dst.height);
			PixCopy(Dst, x, y, Src, srcx, srcy, Src.bitCount);
		}
	}
}

void PicZoom2(TPicRegion& Dst, const TPicRegion& Src)
{
	if ((0 == Dst.width) || (0 == Dst.height)
		|| (0 == Src.width) || (0 == Src.height)) return;

	Dst.bitCount = Src.bitCount;
	TPicRegion_Init(Dst);

	//函数能够处理的最大图片尺寸65536*65536
	unsigned long xrIntFloat_16 = (Src.width << 16) / Dst.width + 1; //16.16格式定点数
	unsigned long yrIntFloat_16 = (Src.height << 16) / Dst.height + 1; //16.16格式定点数
																	   //可证明: (Dst.width-1)*xrIntFloat_16<Src.width成立
	for (unsigned long y = 0; y<Dst.height; ++y)
	{
		for (unsigned long x = 0; x<Dst.width; ++x)
		{
			unsigned long srcx = (x*xrIntFloat_16) >> 16;
			unsigned long srcy = (y*yrIntFloat_16) >> 16;
			PixCopy(Dst, x, y, Src, srcx, srcy, Src.bitCount);
		}
	}
}

//void PicZoom3(TPicRegion& Dst, const TPicRegion& Src)
//{
//	if ((0 == Dst.width) || (0 == Dst.height)
//		|| (0 == Src.width) || (0 == Src.height)) return;
//	unsigned long xrIntFloat_16 = (Src.width << 16) / Dst.width + 1;
//	unsigned long yrIntFloat_16 = (Src.height << 16) / Dst.height + 1;
//	unsigned long dst_width = Dst.width;
//	TARGB* pDstLine = Dst.pdata;
//	unsigned long srcy_16 = 0;
//	for (unsigned long y = 0; y<Dst.height; ++y)
//	{
//		TARGB* pSrcLine = ((TARGB*)((TUInt8*)Src.pdata + Src.byte_width*(srcy_16 >> 16)));
//		unsigned long srcx_16 = 0;
//		for (unsigned long x = 0; x<dst_width; ++x)
//		{
//			pDstLine[x] = pSrcLine[srcx_16 >> 16];
//			srcx_16 += xrIntFloat_16;
//		}
//		srcy_16 += yrIntFloat_16;
//		((TUInt8*&)pDstLine) += Dst.byte_width;
//	}
//}

bool PicRead(TPicRegion * pic, char * fileName)
{
	if (pic == nullptr)
	{
		printf("pic 为空！\n");
		return false;
	}

	FILE * fp = fopen(fileName, "rb");
	if (fp == nullptr)
	{
		printf("打开文件失败！\n");
		return false;
	}
	
	BITMAPFILEHEADER fHead;
	BITMAPINFOHEADER iHead;

	int nRet = fread(&fHead, 1, sizeof(BITMAPFILEHEADER), fp);
	if (nRet != sizeof(BITMAPFILEHEADER))
	{
		printf("读取BMP文件头失败！\n");
		fclose(fp);
		return false;
	}

	printf("-------------------- BMP文件头 -------------------\n");
	printf("文件头      bfType:         %c%c\n", fHead.bfType, fHead.bfType >> 8);
	printf("文件大小    bfSize:         %d\n", fHead.bfSize);
	printf("保留1       bfReserved1:    %d\n", fHead.bfReserved1);
	printf("保留1       bfReserved2:    %d\n", fHead.bfReserved2);
	printf("数据偏移量  bfOffBits:      %d\n", fHead.bfOffBits);
	printf("--------------------------------------------------\n");

	nRet = fread(&iHead, 1, sizeof(BITMAPINFOHEADER), fp);
	if (nRet != sizeof(BITMAPINFOHEADER))
	{
		printf("读取BMP信息失败！\n");
		fclose(fp);
		return false;
	}

	printf("-------------------- BMP信息头 -------------------\n");
	printf("信息头大小  biSize:             %d\n", iHead.biSize);
	printf("图像宽      biWidth:            %d\n", iHead.biWidth);
	printf("图像高      biHeight:           %d\n", iHead.biHeight);
	printf("颜色平面数  biPlanes:           %d\n", iHead.biPlanes);
	printf("像素位宽    biBitCount:         %d\n", iHead.biBitCount);
	printf("图像压缩    biCompression:      %d\n", iHead.biCompression);
	printf("图像大小    biSizeImage:        %d\n", iHead.biSizeImage);
	printf("水平分辨率  biXPelsPerMeter:    %d\n", iHead.biXPelsPerMeter);
	printf("垂直分辨率  biYPelsPerMeter:    %d\n", iHead.biYPelsPerMeter);
	printf("颜色索引数  biClrUsed:          %d\n", iHead.biClrUsed);
	printf("重要颜色索引biClrImportant:     %d\n", iHead.biClrImportant);
	printf("--------------------------------------------------\n");

	pic->width = iHead.biWidth;
	pic->height = iHead.biHeight;
	pic->byte_width = iHead.biWidth * iHead.biBitCount / 8;
	pic->bitCount = iHead.biBitCount;
	pic->picSize = fHead.bfSize - fHead.bfOffBits;
	pic->pdata = new char[pic->picSize];

	nRet = fread(pic->pdata, 1, pic->picSize, fp);
	if (nRet != pic->picSize)
	{
		printf("读取BMP数据失败！\n");
		fclose(fp);
		return false;
	}

	fclose(fp);

	return true;
}

bool PicWrite(TPicRegion * pic, char * fileName)
{
	if (pic == nullptr)
	{
		printf("pic 为空！\n");
		return false;
	}

	FILE * fp = fopen(fileName, "wb");
	if (fp == nullptr)
	{
		printf("打开文件失败！\n");
		return false;
	}

	BITMAPFILEHEADER fHead;
	BITMAPINFOHEADER iHead;

	fHead.bfType = 0x4d42;
	fHead.bfSize = pic->picSize + 54;
	fHead.bfReserved1 = 0;
	fHead.bfReserved2 = 0;
	fHead.bfOffBits = 54;

	iHead.biSize = 40;
	iHead.biWidth = pic->width;
	iHead.biHeight = pic->height;
	iHead.biPlanes = 1;
	iHead.biBitCount = pic->bitCount;
	iHead.biCompression = 0;
	iHead.biSizeImage = 0;
	iHead.biXPelsPerMeter = 0;
	iHead.biYPelsPerMeter = 0;
	iHead.biClrUsed = 0;
	iHead.biClrImportant = 0;

	int nRet = fwrite(&fHead, 1, sizeof(fHead), fp);
	if (nRet != sizeof(fHead))
	{
		printf("写入BMP 文件头错误！\n");
		fclose(fp);
		return false;
	}

	nRet = fwrite(&iHead, 1, sizeof(iHead), fp);
	if (nRet != sizeof(iHead))
	{
		printf("写入BMP 信息头错误！\n");
		fclose(fp);
		return false;
	}

	nRet = fwrite(pic->pdata, 1, pic->picSize, fp);
	if (nRet != pic->picSize)
	{
		printf("写入BMP 数据错误！\n");
		fclose(fp);
		return false;
	}

	fclose(fp);
	return true;
}
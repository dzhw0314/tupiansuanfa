#include "compare.h"
#include "md5.h"
#include <string>

struct TCmpPic
{
	void * pData;
	int width;
	int height;
};

inline unsigned char & CmpPixel(TCmpPic &pic, int x, int y)
{
	return ((unsigned char *)((unsigned char *)pic.pData + y * pic.width))[x];
}

void PixelGray(TCmpPic &dst, int x, int y, TPicRegion & src, int srcx, int srcy, int bitCount)
{
	if(bitCount == 24)
	{
		TARGB24 & srcPixel = Pixels24(src, srcx, srcy);
		unsigned char average = (srcPixel.R * 0.2126 + srcPixel.G * 0.7152 + srcPixel.B * 0.0722);
		CmpPixel(dst, x, y) = average;
	}
	else
	{
		TARGB32 & srcPixel = Pixels32(src, srcx, srcy);\
			int average = (srcPixel.R * 0.2126 + srcPixel.G * 0.7152 + srcPixel.B * 0.0722);
		CmpPixel(dst, x, y) = average;
	}
}

//色彩简化到64级灰度
void RGB2Gray(TPicRegion &src, TCmpPic &dst)
{
	dst.width = src.width;
	dst.height = src.height;
	dst.pData = new char[dst.width * dst.height];

	for (size_t i = 0; i < dst.width; i++)
	{
		for (size_t j = 0; j < dst.height; j++)
		{
			PixelGray(dst, i, j, src, i, j, src.bitCount);
		}
	}
}

//计算平均值
int PicAverage(TCmpPic &dst)
{
	int nAverage = 0;
	for (size_t i = 0; i < dst.width * dst.height; i++)
	{
		nAverage += ((unsigned char *)dst.pData)[i];
	}

	return nAverage / (dst.width * dst.height);
}

//比较像素灰度
void PicBinarization(TCmpPic &dst, int nAverage)
{
	for (size_t i = 0; i < dst.width * dst.height; i++)
	{
		if (((unsigned char *)dst.pData)[i] >= nAverage)
		{
			((unsigned char *)dst.pData)[i] = '1';
		}
		else
		{
			((unsigned char *)dst.pData)[i] = '0';
		}
	}

	((unsigned char *)dst.pData)[dst.width * dst.height] = '\0';
}

void PicBinarization(TPicRegion &pic, int nAverage)
{
	for (size_t i = 0; i < pic.width; i++)
	{
		for (size_t j = 0; j < pic.height; j++)
		{
			if (pic.bitCount == 24)
			{
				TARGB24 & pix = Pixels24(pic, i, j);
				int aver = (pix.R * 0.2126 + pix.G * 0.7152 + pix.B * 0.0722);
				if (aver >= nAverage)
				{
					pix.R = 255;
					pix.G = 255;
					pix.B = 255;
				}
				else
				{
					pix.R = 0;
					pix.G = 0;
					pix.B = 0;
				}
			}
			else
			{
				TARGB32 & pix = Pixels32(pic, i, j);
				int aver = (pix.R * 0.2126 + pix.G * 0.7152 + pix.B * 0.0722);
				if (aver >= nAverage)
				{
					pix.R = 255;
					pix.G = 255;
					pix.B = 255;
				}
				else
				{
					pix.R = 0;
					pix.G = 0;
					pix.B = 0;
				}
			}
		}
	}
}

void GetPicFeature(char * fileName, std::string &strFeature)
{
	strFeature = "";
	
	TPicRegion pic;
	bool bRet = PicRead(&pic, fileName);
	if (bRet == true)
	{
		TPicRegion pic2;
		pic2.width = 64;
		pic2.height = 64;

		PicZoom0(pic2, pic);

		std::string strFileName = "bak32_";
		strFileName += fileName;
		PicWrite(&pic2, (char *)strFileName.c_str());

		TCmpPic dst;
		RGB2Gray(pic2, dst);

		int average = PicAverage(dst);
		PicBinarization(dst, average);

		//PicBinarization(pic, average);
		//PicWrite(&pic, "bakBinary.bmp");

		/*printf("[%s|%d]\n", fileName, average);
		for (int i = dst.height - 1; i >= 0 ; i--)
		{
			for (int j = 0; j < dst.width; j++)
			{
				printf("%c ", ((unsigned char *)dst.pData + i * dst.width)[j]);
			}
			printf("\n");
		}*/
		strFeature = (char *)dst.pData;

		/*Md5Encode md5;
		strFeature = md5.Encode(strFeature).c_str();*/
		printf("Feature:%s\n", strFeature.c_str());
	}
}

//计算汉明距 小于5 相似  大于10 是两张不同的图片
int hamdist(unsigned char *a, unsigned char *b, int len)
{
	int dist = 0;

	for (size_t i = 0; i < len; i++)
	{
		dist += (*a != *b) ? 1 : 0;
		//if (*a != *b)
		//{
		//printf("i = %d *a = %c *b = %c\n", i, *a, *b);
		//}
		a++;
		b++;
	}

	return dist;
}

#include "zoom.h"
#include "compare.h"
#include <windows.h>
#include <math.h>

int main()
{
	/*TPicRegion pic, pic2;
	PicRead(&pic, "1.bmp");

	pic2.width = 352;
	pic2.height = 288;

	PicZoom0(pic2, pic);

	PicWrite(&pic2, "2.bmp");
*/
	std::string strFeature = "";
	GetPicFeature("1.bmp", strFeature);
	std::string strFeature2 = "";
	GetPicFeature("3.bmp", strFeature2);

	int length = strFeature.size();
	int nHam = hamdist((unsigned char *)strFeature.c_str(), (unsigned char *)strFeature2.c_str(), length);

	double similarity = (length - nHam) / (double)length;
	//similarity = pow(similarity, 2);
	printf("hamdist:%d sim:%f%%\n", nHam, similarity * 100);

	system("pause");

	return 0;
}
#pragma once

typedef unsigned char TUInt8; // [0..255]

struct TARGB32      //32 bit color
{
	TUInt8  B, G, R, A;          // A is alpha
};

struct TARGB24      //24 bit color
{
	TUInt8  B, G, R;          // A is alpha
};

struct TPicRegion  //一块颜色数据区的描述，便于参数传递
{
	void*       pdata;         //颜色数据首地址
	long        byte_width;    //一行数据的物理宽度(字节宽度)；
							   //abs(byte_width)有可能大于等于width*sizeof(TARGB32);
	long        width;         //像素宽度
	long        height;        //像素高度
	long        bitCount;      //像素位宽
	long        picSize;       //图像大小
};

void PicZoom0(TPicRegion& Dst, const TPicRegion& Src);
void PicZoom1(TPicRegion& Dst, const TPicRegion& Src);
void PicZoom2(TPicRegion& Dst, const TPicRegion& Src);
//void PicZoom3(const TPicRegion& Dst, const TPicRegion& Src);

bool PicRead(TPicRegion * pic, char * fileName);
bool PicWrite(TPicRegion * pic, char * fileName);

//内部方法
bool TPicRegion_Init(TPicRegion &pic);
inline TARGB24& Pixels24(const TPicRegion& pic, const long x, const long y);
inline TARGB32& Pixels32(const TPicRegion& pic, const long x, const long y);